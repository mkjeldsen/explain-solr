use super::*;
use serde_json::Value;
use std::collections::HashMap;

#[derive(Debug, Deserialize)]
#[serde(untagged)]
pub enum SuccessOrFailure {
    Success(SuccessfulQuery),
    Failure(FailedQuery),
}

#[derive(Debug, Deserialize)]
pub struct SuccessfulQuery {
    pub response: Response,
    pub debug: Debug,
}

#[derive(Debug, Deserialize)]
pub struct Response {
    pub docs: Vec<Document>,
    #[serde(rename = "numFound")]
    pub num_found: i64,
}

type Document = HashMap<String, Value>;

#[derive(Debug, Deserialize)]
pub struct Debug {
    #[serde(with = "tuple_vec_map")]
    pub explain: Vec<(String, Explain)>,
}

#[derive(Debug, Deserialize)]
pub struct Explain {
    #[serde(rename = "match")]
    pub is_match: bool,
    pub value: f64,
    pub description: String,
    pub details: Option<Vec<Explain>>,
}

#[derive(Debug, Deserialize)]
pub struct FailedQuery {
    pub error: QueryError,
}

#[derive(Debug, Deserialize)]
pub struct QueryError {
    pub msg: String,
    pub code: u16,
}
