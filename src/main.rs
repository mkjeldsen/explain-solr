#[macro_use]
extern crate clap;
extern crate regex;
extern crate reqwest;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
#[cfg(test)]
extern crate spectral;
extern crate tuple_vec_map;
extern crate url;

use clap::App;
use clap::Arg;
use reqwest::StatusCode;
use serde_json::Value;
use std::borrow::Cow;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt;
use std::io::prelude::*;
use std::io::BufWriter;
use url::Url;

mod solr;

#[derive(Debug)]
enum StringOfJsonError {
    AnnotateNull,
    AnnotateObject,
    Json(serde_json::Error),
}

impl From<serde_json::Error> for StringOfJsonError {
    fn from(err: serde_json::Error) -> StringOfJsonError {
        StringOfJsonError::Json(err)
    }
}

#[derive(Debug)]
enum ExplainError {
    Http(reqwest::Error),
    Io(std::io::Error),
    Json(serde_json::Error),
    Other(String),
    ParseUrl(url::ParseError),
}

impl From<std::io::Error> for ExplainError {
    fn from(err: std::io::Error) -> ExplainError {
        ExplainError::Io(err)
    }
}

impl From<serde_json::Error> for ExplainError {
    fn from(err: serde_json::Error) -> ExplainError {
        ExplainError::Json(err)
    }
}

impl From<url::ParseError> for ExplainError {
    fn from(err: url::ParseError) -> ExplainError {
        ExplainError::ParseUrl(err)
    }
}

impl From<reqwest::Error> for ExplainError {
    fn from(err: reqwest::Error) -> ExplainError {
        ExplainError::Http(err)
    }
}

impl fmt::Display for ExplainError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use serde_json::error::Category;

        f.write_str("fatal: ")?;
        match *self {
            ExplainError::Http(ref err) => err.fmt(f),
            ExplainError::Io(ref err) => err.fmt(f),
            ExplainError::Json(ref err) => match err.classify() {
                Category::Data => write!(f, "unexpected JSON response: {}", err),
                Category::Eof | Category::Syntax => write!(f, "malformed JSON response: {}", err),
                Category::Io => write!(f, "I/O JSON: {}", err),
            },
            ExplainError::Other(ref s) => s.fmt(f),
            ExplainError::ParseUrl(ref err) => err.fmt(f),
        }
    }
}

impl std::error::Error for ExplainError {
    fn description(&self) -> &str {
        match *self {
            ExplainError::Http(ref err) => err.description(),
            ExplainError::Io(ref err) => err.description(),
            ExplainError::Json(ref err) => err.description(),
            ExplainError::Other(ref err) => err,
            ExplainError::ParseUrl(ref err) => err.description(),
        }
    }
}

fn main() {
    // http://localhost:8983/solr/demo/select?defType=edismax&q=name:test&fl=id,name&debugQuery=on&debug.explain.structured=true&rows=10
    // http://localhost:8983/solr/demo/select?fl=id,name&q=name:solr+name:memory&group=true&group.field=manu_id_s
    // http://localhost:8983/solr/demo/select?fl=id,name&q=name:solr+name:memory&group=true&group.field=manu_id_s&group.main=true
    // http://localhost:8983/solr/demo/select?fl=id,name&q=name:solr+name:memory&group=true&group.field=manu_id_&group.main=true

    let m = App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .arg(
            Arg::with_name("annotate")
                .short("a")
                .long("annotate")
                .takes_value(true)
                .default_value(r#""""#)
                .hide_default_value(true)
                // se tw=50
                .help(
                    r#"
Annotates "explain" entries with the values from
these fields
"#.trim(),
                )
                // se tw=72
                .long_help(
                    r#"
Annotates every "explain" entry in the output with the values from these
fields for the corresponding document. FIELDS is a comma-separated list
of field names, like the value of Solr's "fl" query parameter. If QUERY
includes an "fl" query parameter its value will be modified to include
FIELDS. If a document does not contain a requested field, said field
produces no output for the given document.

Values will be coerced into a string representation to prevent ambiguity
between values and value delimiters. If a field is multivalued, only the
first value--or an empty string if the field is empty--will be displayed
Special characters will be escaped where necessary.
"#.trim(),
                ).value_name("FIELDS"),
        ).arg(
            Arg::with_name("query")
                .required(true)
                .help("Explains this Solr query")
                .long_help(
                    r#"
Pretty-prints the "explain" output produced by Solr for the query
defined by this URL. The query does not have to include special
debugging parameters or use a specific response writer, explain-solr
will modify the query as necessary in order to parse the response.
"#.trim(),
                ).value_name("QUERY"),
        ).get_matches();
    let query = m.value_of("query").unwrap();

    let annotations = m.value_of("annotate").unwrap();

    std::process::exit(match run(&query, &annotations) {
        Ok(()) => 0,
        Err(ref err) => match *err {
            ExplainError::Io(ref inner) if inner.kind() == std::io::ErrorKind::BrokenPipe => {
                // 128 + 13
                141
            }
            _ => {
                eprintln!("{}", err);
                1
            }
        },
    });
}

fn run(url: &str, annotations: &str) -> Result<(), ExplainError> {
    let idx = url.find('?');
    if idx == None {
        return Err(ExplainError::Other("no query in URL".to_string()));
    }
    let host_and_path = &url[..idx.unwrap()];
    let parsed_url = Url::parse(&url)?;
    let parsed_url = {
        let query_pairs = query_builder(parsed_url.query_pairs(), &annotations);
        Url::parse_with_params(host_and_path, query_pairs)?
    };

    let wtr = std::io::stdout();
    let mut wtr = BufWriter::new(wtr.lock());
    writeln!(wtr, "Query: {}\n", parsed_url)?;

    let response = reqwest::get(parsed_url);

    match response {
        Ok(mut response) => {
            // Can't use Response::error_for_status() because we need to parse Solr's error message.
            match response.status() {
                StatusCode::Ok | StatusCode::BadRequest => {
                    let body = response.text()?;
                    let annotations: Vec<&str> = annotations.split(',').collect();

                    parse_response(&mut wtr, &body, &annotations)
                }
                code => Err(ExplainError::Other(code.to_string())),
            }
        }
        Err(e) => Err(ExplainError::Other(
            e.get_ref().expect("inner reqwest error").to_string(),
        )),
    }
}

fn query_builder<'q>(
    query_pairs: url::form_urlencoded::Parse<'q>,
    annotations: &str,
) -> Vec<(Cow<'q, str>, Cow<'q, str>)> {
    let reserved: HashSet<&str> = ["wt", "debugQuery", "debug.explain.structured", "group.main"]
        .iter()
        .cloned()
        .collect();

    let mut has_group = false;
    let id_pattern = regex::Regex::new(r#"(?-u:\bid\b)"#).unwrap();
    let mut query_pairs: Vec<(Cow<'q, str>, Cow<'q, str>)> = query_pairs
        .filter_map(|(k, v)| {
            if k == "fl" {
                let v = if v.contains(&annotations) {
                    if id_pattern.is_match(&v) {
                        v
                    } else {
                        let mut v = v.into_owned();
                        v.push_str(",id");
                        v.into()
                    }
                } else {
                    let mut v = v.into_owned();
                    v.push(',');
                    v.push_str(&annotations);
                    if !id_pattern.is_match(&v) {
                        v.push_str(",id");
                    }
                    v.into()
                };
                Some((k, v))
            } else if reserved.contains(&*k) {
                None
            } else {
                if k == "group" {
                    has_group = true;
                }
                Some((k, v))
            }
        }).collect();

    query_pairs.push(("wt".into(), "json".into()));
    query_pairs.push(("debugQuery".into(), "on".into()));
    query_pairs.push(("debug.explain.structured".into(), "true".into()));

    if has_group {
        query_pairs.push(("group.main".into(), "true".into()));
    }

    query_pairs
}

fn parse_response<W>(wtr: &mut W, response: &str, annotations: &[&str]) -> Result<(), ExplainError>
where
    W: Write,
{
    let query: solr::SuccessOrFailure = serde_json::from_str(&response)?;
    let query = match query {
        solr::SuccessOrFailure::Success(ref q) => q,
        solr::SuccessOrFailure::Failure(q) => {
            return Err(ExplainError::Other(q.error.msg));
        }
    };

    if query.response.num_found == 0 {
        writeln!(wtr, "No results")?
    }

    let mut annotation_of_docid: HashMap<&str, String> =
        HashMap::with_capacity(query.response.docs.len());
    if !annotations.is_empty() {
        for doc in &query.response.docs {
            // Actually, Solr doesn't guarantee that there'll be a unique field, that it will be
            // named "id", or that it will be a string: https://wiki.apache.org/solr/SchemaXml#The_Unique_Key_Field
            let id = doc
                .get("id")
                .expect("field 'id' always in document")
                .as_str()
                .expect("field 'id' is string");
            let mut pretty = Vec::with_capacity(annotations.len());

            for annotation in annotations {
                if let Some(val) = doc.get(*annotation) {
                    match docvalue_to_string(val) {
                        Ok(v) => pretty.push(v),
                        Err(StringOfJsonError::AnnotateNull) => {
                            return Err(ExplainError::Other(format!(
                                r#"unexpected null value for field "{}""#,
                                annotation
                            )))
                        }
                        Err(StringOfJsonError::AnnotateObject) => {
                            return Err(ExplainError::Other(format!(
                                r#"unexpected object value for field "{}": {}"#,
                                annotation, val
                            )))
                        }
                        Err(StringOfJsonError::Json(ref e)) => {
                            return Err(ExplainError::Other(format!("{}", e)))
                        }
                    }
                }
            }

            if !pretty.is_empty() {
                annotation_of_docid.insert(id, pretty.join(", "));
            }
        }
    }

    for (ref id, ref ex) in &query.debug.explain {
        if let Some(pretty) = annotation_of_docid.get(id.as_str()) {
            writeln!(wtr, "{} ({})", id, pretty)?
        } else {
            writeln!(wtr, "{}", id)?
        }
        visit_explain(wtr, 1, ex)?
    }

    Ok(())
}

fn docvalue_to_string(val: &Value) -> Result<String, StringOfJsonError> {
    use serde_json::ser::to_string as outlined_string;

    match *val {
        Value::String(_) => Ok(val.to_string()),
        Value::Array(ref arr) => if arr.is_empty() {
            Ok(r#""""#.to_string())
        } else {
            docvalue_to_string(&arr[0])
        },
        Value::Bool(ref v) => Ok(outlined_string(&v.to_string())?),
        Value::Number(ref v) => Ok(outlined_string(&v.to_string())?),
        Value::Object(_) => Err(StringOfJsonError::AnnotateObject),
        Value::Null => Err(StringOfJsonError::AnnotateNull),
    }
}

fn visit_explain<W>(wtr: &mut W, level: usize, explain: &solr::Explain) -> Result<(), ExplainError>
where
    W: Write,
{
    writeln!(
        wtr,
        "{0:1$}{2}  =  {3}",
        ' ',
        2 * level,
        explain.value,
        explain.description.replace('\n', "")
    )?;

    if let Some(ref details) = explain.details {
        for d in details {
            visit_explain(wtr, level + 1, d)?
        }
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;
    use spectral::assert_that;
    use spectral::prelude::*;
    use url::form_urlencoded::parse;

    fn mkwtr() -> BufWriter<Vec<u8>> {
        BufWriter::new(Vec::new())
    }

    #[test]
    fn query_always_has_structured_debug_in_json() {
        let query_pairs = query_builder(parse("".as_bytes()), "");
        assert_that(&query_pairs).is_equal_to(&vec![
            ("wt".into(), "json".into()),
            ("debugQuery".into(), "on".into()),
            ("debug.explain.structured".into(), "true".into()),
        ]);
    }

    #[test]
    fn query_interface_cannot_be_overridden() {
        let query_pairs = query_builder(
            parse("wt=xml&debugQuery=off&debug.explain.structured=false".as_bytes()),
            "",
        );
        assert_that(&query_pairs).is_equal_to(&vec![
            ("wt".into(), "json".into()),
            ("debugQuery".into(), "on".into()),
            ("debug.explain.structured".into(), "true".into()),
        ]);
    }

    #[test]
    fn query_with_grouping_flattens_groups() {
        // group.main loses a little information (groupwise document counts) but doesn't change
        // relevancy or ordering. It's a simple way to avoid building a more complex integration.
        let query_pairs = query_builder(parse("group=on&group.main=false".as_bytes()), "");
        let relevant: Vec<(Cow<'_, str>, Cow<'_, str>)> = query_pairs
            .into_iter()
            .filter(|(k, _)| k == "group" || k == "group.main")
            .collect();
        assert_that(&relevant).is_equal_to(&vec![
            ("group".into(), "on".into()),
            ("group.main".into(), "true".into()),
        ]);
    }

    #[test]
    fn query_without_fl_never_has_fl() {
        // Solr includes all fields by default, fl only narrows selection. Leave that to the user.
        let query_pairs = query_builder(parse("".as_bytes()), "");
        let fl = query_pairs.into_iter().find(|(k, _)| k == "fl");
        assert_that(&fl).is_none();
    }

    #[test]
    fn query_with_fl_includes_id() {
        // Can't annotate without id, prevent user from self-sabotage.
        let query_pairs = query_builder(parse("fl=fiddle".as_bytes()), "");
        let fl = query_pairs
            .into_iter()
            .find(|(k, _)| k == "fl")
            .expect("some fl");
        assert_that(&&*fl.1).is_equal_to(&"fiddle,id");
    }

    #[test]
    fn query_with_fl_includes_all_annotations() {
        // Solr only returns distinct fl values; duplicates in request is okay.
        let query_pairs = query_builder(parse("fl=fiddle,name".as_bytes()), "name,foo,bar");
        let fl = query_pairs
            .into_iter()
            .find(|(k, _)| k == "fl")
            .expect("some fl");
        assert_that(&&*fl.1).is_equal_to(&"fiddle,name,name,foo,bar,id");
    }

    #[test]
    fn query_with_fl_including_all_annotations_does_not_duplicate_fields() {
        let query_pairs = query_builder(parse("fl=id,name".as_bytes()), "name");
        let fl = query_pairs
            .into_iter()
            .find(|(k, _)| k == "fl")
            .expect("some fl");
        assert_that(&&*fl.1).is_equal_to(&"id,name");
    }

    #[test]
    fn docvalue_to_string_of_primitive_yields_quoted_string_representation() {
        let some_number = serde_json::Number::from_f64(42.37).unwrap();

        let some_primitives = vec![
            (Value::String("some string".into()), r#""some string""#),
            (Value::Number(some_number), r#""42.37""#),
            (Value::Bool(true), r#""true""#),
            (Value::Bool(false), r#""false""#),
        ];

        let stringy_primitives: Vec<(Result<String, _>, &str)> = some_primitives
            .iter()
            .map(|(v, expect)| (docvalue_to_string(&v), *expect))
            .collect();

        for (actual, expect) in stringy_primitives {
            assert_that(&actual).is_ok();
            assert_that(&actual.unwrap().as_str()).is_equal_to(&expect);
        }
    }

    #[test]
    fn docvalue_to_string_of_empty_array_yields_emptystring_string() {
        let res = docvalue_to_string(&Value::Array(Vec::new()));

        assert_that(&res).is_ok();
        assert_that(&res.unwrap().as_str()).is_equal_to(r#""""#);
    }

    #[test]
    fn docvalue_to_string_of_populated_array_tries_first_item() {
        let res = docvalue_to_string(&Value::Array(vec![
            Value::String("first".into()),
            Value::String("second".into()),
        ]));
        assert_that(&res).is_ok();
        assert_that(&res.unwrap().as_str()).is_equal_to(r#""first""#);
    }

    #[test]
    fn docvalue_to_string_of_object_is_err() {
        let mut m = serde_json::Map::new();
        m.insert("foo".into(), Value::String("bar".into()));
        let res = docvalue_to_string(&Value::Object(m));

        assert_that(&res).is_err();
    }

    #[test]
    fn docvalue_to_string_of_null_is_err() {
        let res = docvalue_to_string(&Value::Null);

        assert_that(&res).is_err();
    }

    #[test]
    fn parse_response_of_bad_syntax_json_is_malformed() {
        let mut wtr = mkwtr();
        let res = parse_response(&mut wtr, "}", &[]);

        assert_that(&res).is_err();

        let stderr = format!("{}", res.unwrap_err());
        assert_that(&stderr).contains("fatal: malformed JSON");
        assert_that(&stderr).contains("expected value");

        let stdout = wtr.into_inner().unwrap();
        assert_that(&stdout).is_empty();
    }

    #[test]
    fn parse_response_of_nonsense_json_is_malformed() {
        let mut wtr = mkwtr();
        let res = parse_response(&mut wtr, "42", &[]);

        assert_that(&res).is_err();

        let stderr = format!("{}", res.unwrap_err());
        assert_that(&stderr).contains("fatal: unexpected JSON");
        assert_that(&stderr).contains("did not match");

        let stdout = wtr.into_inner().unwrap();
        assert_that(&stdout).is_empty();
    }

    #[test]
    fn parse_response_of_nondebug_json_is_malformed() {
        let mut wtr = mkwtr();
        let res = parse_response(&mut wtr, include_str!("../resources/non-debug.json"), &[]);

        assert_that(&res).is_err();

        let stderr = format!("{}", res.unwrap_err());
        assert_that(&stderr).contains("fatal: unexpected JSON");
        assert_that(&stderr).contains("did not match");

        let stdout = wtr.into_inner().unwrap();
        assert_that(&stdout).is_empty();
    }

    #[test]
    fn parse_response_of_error_response_yields_solr_error_message() {
        let mut wtr = mkwtr();
        let res = parse_response(
            &mut wtr,
            include_str!("../resources/malformed-query.json"),
            &[],
        );

        assert_that(&res).is_err();

        let stderr = format!("{}", res.unwrap_err());
        assert_that(&stderr).contains("fatal: undefined field nonexistent");

        let stdout = wtr.into_inner().unwrap();
        assert_that(&stdout).is_empty();
    }

    #[test]
    fn parse_response_with_no_documents_succeeds() {
        let mut wtr = mkwtr();
        let res = parse_response(
            &mut wtr,
            include_str!("../resources/no-documents.json"),
            &[],
        );

        assert_that(&res).is_ok();

        let stdout = wtr.into_inner().map(String::from_utf8).unwrap().unwrap();
        assert_that(&stdout).contains("No results");
    }

    #[test]
    fn parse_response_with_no_annotation_succeeds() {
        let mut wtr = mkwtr();
        let res = parse_response(&mut wtr, include_str!("../resources/documents.json"), &[]);

        assert_that(&res).is_ok();

        let stdout = wtr.into_inner().map(String::from_utf8).unwrap().unwrap();
        assert_that(&stdout).contains("IW-02");
        assert_that(&stdout).contains("F8V7067-APL-KIT");
    }

    #[test]
    fn parse_response_with_single_annotation_succeeds() {
        let mut wtr = mkwtr();
        let res = parse_response(
            &mut wtr,
            include_str!("../resources/documents.json"),
            &["manu_str"],
        );

        assert_that(&res).is_ok();

        let stdout = wtr.into_inner().map(String::from_utf8).unwrap().unwrap();
        assert_that(&stdout).contains(r#"IW-02 ("Belkin")"#);
        assert_that(&stdout).contains(r#"MA147LL/A ("Apple Computer Inc.")"#);
    }

    #[test]
    fn parse_response_with_multiple_annotations_succeeds() {
        let mut wtr = mkwtr();
        let res = parse_response(
            &mut wtr,
            include_str!("../resources/documents.json"),
            &["manu_str", "id"],
        );

        assert_that(&res).is_ok();

        let stdout = wtr.into_inner().map(String::from_utf8).unwrap().unwrap();
        assert_that(&stdout).contains(r#"IW-02 ("Belkin", "IW-02")"#);
    }

    #[test]
    fn parse_response_with_annotation_missing_from_some_documents_succeeds() {
        let mut wtr = mkwtr();
        let res = parse_response(
            &mut wtr,
            include_str!("../resources/documents.json"),
            &["manu_str", "includes"],
        );

        assert_that(&res).is_ok();

        let stdout = wtr.into_inner().map(String::from_utf8).unwrap().unwrap();
        assert_that(&stdout).contains(r#"IW-02 ("Belkin")"#);
        assert_that(&stdout)
            .contains(r#"MA147LL/A ("Apple Computer Inc.", "earbud headphones, USB cable")"#);
    }

    #[test]
    fn parse_response_with_annotation_of_null_is_malformed() {
        let mut wtr = mkwtr();
        let res = parse_response(
            &mut wtr,
            include_str!("../resources/bad-field-data.json"),
            &["some_null"],
        );

        assert_that(&res).is_err();

        let stderr = format!("{}", res.unwrap_err());
        assert_that(&stderr).contains("fatal: unexpected null");
        assert_that(&stderr).contains("some_null");

        let stdout = wtr.into_inner().unwrap();
        assert_that(&stdout).is_empty();
    }

    #[test]
    fn parse_response_with_annotation_of_object_is_malformed() {
        let mut wtr = mkwtr();
        let res = parse_response(
            &mut wtr,
            include_str!("../resources/bad-field-data.json"),
            &["some_object"],
        );

        assert_that(&res).is_err();

        let stderr = format!("{}", res.unwrap_err());
        assert_that(&stderr).contains("fatal: unexpected object");
        assert_that(&stderr).contains("some_object");

        let stdout = wtr.into_inner().unwrap();
        assert_that(&stdout).is_empty();
    }
}
