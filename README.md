# explain-solr

`explain-solr` executes a Solr query in explain mode and pretty-prints the
explain output. The document IDs in the explain output can be annotated with
field values from the corresponding document for increased recognizability.
This makes it easier to investigate relevancy scoring than by reading the raw
_explain_ output or [the `[explain]` document transformer][doc-transformer];
indeed, I made `explain-solr` primarily because I got tired of manually
cross-referencing opaque document IDs.

`explain-solr` doesn't need a specially formatted query, only a valid one; it
will make non-destructive modifications as necessary in order to be able to
parse the response. This enables you to take a query straight out of Solr's
query log and explain it against a Solr instance.

Note that building `explain-solr` requires the OpenSSL 1.0.x headers; packages
`libssl-dev` in Debian and `openssl-devel` in Fedora, although that version is
presently being replaced by version 1.1.

## Examples

For the sake of brevity and consistency there are no examples in the README.
Instead, the `samples/` directory contains

1. examples of queries;
1. output produced by `explain-solr` for those queries; and
1. the raw Solr _explain_ responses parsed by `explain-solr` for comparison
   purposes.

Those examples were made with the Solr 7.4 _solr-demo_ example provided in the
official Solr Docker images:

```sh
$ docker run --detach --publish 8983:8983 solr:7.4-alpine solr-demo
```

## Disclaimer

`explain-solr` is made for Solr 7.4. It has been superficially validated
against versions 6.6, 5.5, and 4.8 but does not strive for compatibility with
multiple versions. It's output does not form a contract.

This project is not affiliated with the Apache Software Foundation, the Apache
Lucene project, or Apache Solr. It is unrelated to, and the idea conceived of
independently of, the spiritually similar but defunct https://explain.solr.pl/.

[doc-transformer]: https://lucene.apache.org/solr/guide/7_4/transforming-result-documents.html#explain-explainaugmenterfactory "[explain] document transformer"
